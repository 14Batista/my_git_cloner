
# My Git Cloner

Automatizacion del proceso de otro respaldo a proyectos de git para novatos


## Funciones Generales
En alguna carpeta del sistema por ejemplo "Documents" crea un archivo llamado 

```bash
my_cloner_functions.ps1
```

## Configuracion del Profile

Desde el powershell abrir el archivo del profile

```bash
notepad $PROFILE
```

o para crearlo si no existe 

```bash
New-Item -Path $PROFILE -ItemType File -Force
```

Pegar y modificar la siguiente porcion de codigo

```bash
# Direccion de el archivo.
. 'C:\Users\<<direccion>>\my_cloner_functions.ps1

function Clonar_Repositorio {
    param ( [string]$branch = "Developer" )
    ClonarRepositorio -branch $branch
}

Set-Alias xclone Clonar_Repositorio

function Manejador_Rutas {
    param ( [string]$gitUrl, [switch]$d )
    if ($d) { ManejadorRutas -gitUrl $gitUrl -d }
    else { ManejadorRutas -gitUrl $gitUrl }
}

Set-Alias xroute Manejador_Rutas
```

## Funciones Generales

Agregar el siguiente contenido al archivo my_cloner_functions.ps1: 

```bash
function ObtenerRepositorioURL {

    param (
        [string]$carpetaPadre,
        [string]$carpetaHijo,
        [string]$archivoConfig = "my_cloner_repositories.txt" 
    )

    #Buscar y leer el archivo de las rutas de los repositorios
    $archivoConfigPath = Split-Path -Path $profile -Parent
    $archivoConfigPath = Join-Path -Path $archivoConfigPath -ChildPath $archivoConfig

    if (-not (Test-Path -Path $archivoConfigPath)) {
        Write-Host "El archivo de configuracion '$archivoConfig' no existe."
        return $null
    }

    $configuraciones = Get-Content -Path $archivoConfigPath
    $repositorios = @{}

    foreach ($linea in $configuraciones) {
        $partes = $linea -split "="
        if ($partes.Count -eq 2) {
            $repositorios[$partes[0]] = $partes[1]
        }
    }

    #Validar que la carpeta actual pertenece a un repositorio valido
    $clave = "$carpetaPadre/$carpetaHijo"
    
    $padresValidos = $repositorios.Keys | ForEach-Object { $_.Split('/')[0] } | Select-Object -Unique
    $hijosValidos = $repositorios.Keys | ForEach-Object { $_.Split('/')[1] } | Select-Object -Unique
        
    if (-not ($padresValidos -contains $carpetaPadre)) {
        Write-Host "$carpetaPadre no es un directorio valido."
        return $null
    }

    if (-not ($hijosValidos -contains $carpetaHijo)) {
        Write-Host "$carpetaHijo no es un proyecto valido."
        return $null
    }
        
    if ($repositorios.ContainsKey($clave)) {
        return $repositorios[$clave]
    } 

}

function CrearRespaldo {

    param (
        [string]$carpetaPadre,
        [string]$carpetaHijo
    )

    if (Test-Path -Path $carpetaHijo -PathType Container) {
        $contenido = Get-ChildItem -Path $carpetaHijo
        if ($contenido.Count -gt 0) {

            # Borrar la carpeta node_modules si existe
            $nodeModulesPath = Join-Path -Path $carpetaHijo -ChildPath "node_modules"
            if (Test-Path -Path $nodeModulesPath -PathType Container) {
                Remove-Item -Path $nodeModulesPath -Recurse -Force
                Write-Host "La carpeta node_modules ha sido eliminada."
            }
            
            # Agregar la fecha actual al nombre de la carpeta hijo
            $fecha = Get-Date -Format "MMddyyHHmm"
            $nuevoNombre = [System.IO.Path]::GetFileName($carpetaHijo) + "_" + $fecha
            $nuevoNombreFull = Join-Path -Path $carpetaPadre -ChildPath $nuevoNombre
            
            try {
                Set-Location -Path $carpetaPadre
                
                Rename-Item -Path $carpetaHijo -NewName $nuevoNombre
                # Verificar si la carpeta fue renombrada exitosamente
                $validado = Test-Path -Path $nuevoNombreFull -PathType Container
                if ($validado) {
                    Write-Host "Respaldo Creado Correctamente: $nuevoNombre"
                    return $true
                }
            
                Write-Host "Error: Error al Crear el respaldo."
                return $false
                
            }
            catch {
                Write-Host "Error al crear el respaldo: $_"
                return $false
            }
        }
       
        # Write-Host "La carpeta esta vacia."
        return $true
        
    }
  
    Write-Host "La carpeta '$carpetaHijo' no existe."
    return $false
    
}

function RealizarClonado { 
    param ( 
        [string]$branch, 
        [string]$url, 
        [string]$carpetaHijo 
    ) 
        
    try { 
        # Clonar el repositorio
        git clone -b $branch $url $carpetaHijo 
        $validado = Test-Path -Path $carpetaHijo -PathType Container 

        if ($validado) {
            $packagePath = Join-Path -Path $carpetaHijo -ChildPath "package.json" 
            
            # si se clono el proyecto y tiene package.json instala las dependencias
            if (Test-Path $packagePath) { 
                Set-Location -Path $carpetaHijo 
                npm install 
                Write-Host "Proyecto clonado exitosamente"
                return $true
            }
            else { 
                Write-Host "El archivo package.json no existe."
                return $false
            } 
        } 
        
        return $false
    }
    catch { Write-Host "error al clonar el ropositorio $_" return $false } 
}

function ClonarRepositorio {
    param (
        [string]$branch = 'main',
        [string]$url = $null
    )

    # 1. Capturar la carpeta actual como hijo y la carpeta padre
    $carpetaActual = Get-Item -Path "."

    $carpetaPadre = $carpetaActual.Parent.FullName
    $carpetaHijo = $carpetaActual.FullName
    $carpetaPadreName = $carpetaActual.Parent.Name
    $carpetaHijoName = $carpetaActual.Name
   
    # 2. Obtener la URL del repositorio
    $giturl = ObtenerRepositorioURL -carpetaPadre $carpetaPadreName -carpetaHijo $carpetaHijoName
    if (-not $giturl) { return }

    # 3. Crear respaldo del proyecto
    $respaldado = CrearRespaldo -carpetaPadre $carpetaPadre -carpetaHijo $carpetaHijo
    if (-not $respaldado) { return }

    # 4. Clonar el repositorio
    $clonado = RealizarClonado -branch $branch -carpetaHijo $carpetaHijo -url $giturl
    if (-not $clonado) { return }
    
    Write-Host "--------------------Completed---"
}

function ManejadorRutas {
    param (
        [string]$gitUrl,
        [switch]$d
    )
        
    $carpetaActual = Get-Item -Path "."
    $clave = $carpetaActual.Parent.Name + '/' + $carpetaActual.Name
    
    $archivoConfig = "my_cloner_repositories.txt"
    $archivoConfigPath = Split-Path -Path $profile -Parent
    $archivoConfigPath = Join-Path -Path $archivoConfigPath -ChildPath $archivoConfig

    # Crear el archivo de rutas si no existe
    if (-not (Test-Path -Path $archivoConfigPath)) {
        New-Item -Path $archivoConfigPath -ItemType File -Force
        Write-Host "El archivo de configuracion '$archivoConfig' creado."

        # Confirmar que se creo el archivo de rutas
        if (-not (Test-Path -Path $archivoConfigPath)) {
            Write-Host "El archivo de configuracion '$archivoConfig' no existe."
            return $false
        }
    }    
        
    $listaRutas = Get-Content -Path $archivoConfigPath

    if ($d) {
        #Borrar la Ruta
        $contenidoActualizado = $listaRutas | Where-Object { -not ($_ -match "^$clave=") }
        $contenidoActualizado | Set-Content $archivoConfigPath

        Write-Host "Proyecto eliminado"
        Write-Host "Ruta $clave"
        Write-Host "Repositorio $gitUrl"
    }
    else {
        #Insertar la Ruta y si existe actuaizarla
        $nuevaRuta = $clave + '=' + $gitUrl

        if (-not $gitUrl) {
            Write-Host "Ingrese la url del proyecto"
            return $false
        }

        $rutaExist = $false
        for ($i = 0; $i -lt $listaRutas.Length; $i++) {
            if ($listaRutas[$i] -match "^$clave=") {
                $listaRutas[$i] = $nuevaRuta
                $rutaExist = $true
                break
            }
        }

        if (-not $rutaExist) {
            $listaRutas += "`r`n$nuevaRuta"
        }

        $listaRutas | Set-Content $archivoConfigPath

        Write-Host "Nuevo proyecto agregado"
        Write-Host "Ruta $clave"
        Write-Host "Repositorio $gitUrl"
    }

    return $true
}
```

